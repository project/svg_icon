<?php

namespace Drupal\svg_icon\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_library\Form\FileUploadForm;

/**
 * Defines a class for a media library add form.
 */
class MediaLibraryAddForm extends FileUploadForm {

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {
    $element = parent::buildInputElement($form, $form_state);
    if ($this->currentUser()->hasPermission('use svg icon upload widget')) {
      $element['container']['upload']['#disabled'] = TRUE;
      $element['container']['upload']['#suffix'] = t('Insufficient permissions to upload a new SVG, please use an existing one instead.');
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function createMediaFromValue(MediaTypeInterface $media_type, EntityStorageInterface $media_storage, $source_field_name, $source_field_value) {
    $media = parent::createMediaFromValue($media_type, $media_storage, $source_field_name, $source_field_value);
    $media->{$source_field_name}->svg_id = '';
    return $media;
  }

}
