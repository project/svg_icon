<?php

namespace Drupal\svg_icon\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'svg_icon' field type.
 *
 * @FieldType(
 *   id = "svg_icon",
 *   label = @Translation("Svg icon"),
 *   description = @Translation("Custom SVG Icon field type plugin"),
 *   default_widget = "svg_icon_widget",
 *   default_formatter = "svg_icon_formatter",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {}
 * )
 */
class SvgIcon extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $defaults = parent::defaultFieldSettings();
    $defaults['file_extensions'] = 'svg';
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['svg_id'] = [
      'type' => 'varchar',
      'length' => '255',
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['svg_id'] = DataDefinition::create('string')
      ->setLabel(t('SVG Id'))
      ->setRequired(TRUE);
    return $properties;
  }

}
