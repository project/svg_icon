<?php

namespace Drupal\svg_icon\Plugin\media\Source;

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Plugin\media\Source\Image;

/**
 * Provides media type plugin for SVG Image.
 *
 * @MediaSource(
 *   id = "svg_icon",
 *   label = @Translation("SVG Icon"),
 *   description = @Translation("Provides business logic and metadata for svg icons."),
 *   allowed_field_types = {"svg_icon"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   forms = {
 *     "media_library_add" = "\Drupal\svg_icon\Form\MediaLibraryAddForm",
 *   }
 * )
 */
class SvgIcon extends Image {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $bundle = $form_state->getFormObject()->getEntity();

    foreach ($this->entityFieldManager->getFieldDefinitions('media', $bundle->id()) as $field_name => $field) {
      if ($field->getType() === 'svg_icon' && !$field->getFieldStorageDefinition()->isBaseField()) {
        $form['source_field']['#options'][$field_name] = $field->getLabel();
      }
    }

    unset($form['gather_exif']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

}
